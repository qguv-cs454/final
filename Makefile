LDIR = .latex
FNAME = final
LC = ./latexrun

.PHONY: FORCE
$(FNAME).pdf: FORCE
	$(LC) -O $(LDIR) $(FNAME).tex

.PHONY: sweep
sweep:
	rm -rf $(LDIR)

.PHONY: clean
clean: sweep
	rm -rf $(FNAME).pdf
